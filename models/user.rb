# frozen_string_literal: true

class User < Sequel::Model
  many_to_many :chats, left_id: :user_id, right_id: :chat_id, join_table: :users_to_chats
end
