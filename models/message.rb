# frozen_string_literal: true

class Message < Sequel::Model
  many_to_one :chat
end
