# frozen_string_literal: true

class Chat < Sequel::Model
  many_to_many :users, left_id: :chat_id, right_id: :user_id, join_table: :users_to_chats
  one_to_many :messages
end
