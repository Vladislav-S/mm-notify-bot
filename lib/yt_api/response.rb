# frozen_string_literal: true

module YtApi
  class Response
    attr_reader :response

    def initialize(response)
      @response = response

      logger.info("#{self.class.name} | status: #{response.code} | body: #{body}")
    end

    def body
      @body ||= JSON.parse(response.body)
    end

    def failure
      response.code != "200"
    end

    def success
      response.code == "200"
    end

    def unauthorized?
      response.code == "401"
    end

    def error_message
      return unless failure
      body["message"]
    end

    def error_code
      return unless failure

      response.code
    end

    def total_pages
      response.fetch('X-Total-Pages', 0).to_i
    end

    def total_count
      response.fetch('X-Total-Count', 0).to_i
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
