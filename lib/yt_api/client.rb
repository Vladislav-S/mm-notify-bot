# frozen_string_literal: true

require_relative 'base'

module YtApi
  class Client < Base
    def issue_comments(issue_id)
      make_request(:get, "issues/#{issue_id}/comments")
    end

    def queues
      make_request_all_pages(:get, "queues")
    end

    def get_triggers(queue_key)
      make_request(:get, "queues/#{queue_key}/triggers")
    end

    def get_trigger(queue_key, trigger_id)
      make_request(:get, "queues/#{queue_key}/triggers/#{trigger_id}")
    end

    def create_trigger(queue_key, params)
      make_request(:post, "queues/#{queue_key}/triggers", params)
    end
  end
end
