# frozen_string_literal: true

require_relative 'response'

module YtApi
  class Base
    private

    CURRENT_API = "/v2/"
    FIRST_PAGE = 1

    def make_request(method, path, params = {})
      request_url = URI(base_url + CURRENT_API + path)

      case method
      when :post
        post_request(request_url, params)
      when :get
        get_request(request_url, params)
      end
    end

    def make_request_all_pages(method, path, params = {})
      page = FIRST_PAGE
      response_array = []
      loop do
        response = make_request(method, path, params.merge({ page: }))
        break if response.failure || response.body.empty?

        page += 1
        response_array += response.body
      end

      # FIXME: лучше возвращать класс RESPONSE с объединенным телом
      response_array
    end

    def get_request(request_url, params)
      query_params = params.map { |key, val| "#{key}=#{val}" }.join("&")
      request_url.query = query_params unless query_params.empty?
      logger.info("#{self.class.name} GET #{request_url}")
      Net::HTTP.start(request_url.host, request_url.port, use_ssl: true) do |http|
        request = Net::HTTP::Get.new(request_url)
        request['Content-Type'] = 'application/json'
        request['accept'] = 'application/json'
        request['Authorization'] = "Bearer #{token}"
        request['X-Cloud-Org-ID'] = x_org_id

        Response.new(http.request(request))
      end
    end

    def post_request(request_url, body)
      logger.info("#{self.class.name} POST #{request_url} | #{body}")

      Net::HTTP.start(request_url.host, request_url.port, use_ssl: true) do |http|
        request = Net::HTTP::Post.new(request_url)
        request['Content-Type'] = 'application/json'
        request['accept'] = 'application/json'
        request['Authorization'] = "Bearer #{token}"
        request['X-Cloud-Org-ID'] = x_org_id

        request.body = body.to_json unless body.empty?

        Response.new(http.request(request))
      end
    end

    def x_org_id
      @x_org_id ||= ENV.fetch("YT_X_ORG_ID").freeze
    end

    def base_url
      @base_url ||= ENV.fetch("YT_URL").freeze
    end

    def token
      # FIXME: токен пользователя протухает через сутки. Нужен сервисный или федеративный
      @token ||= ENV.fetch("YT_IAM_KEY").freeze
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
