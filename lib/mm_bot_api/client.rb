# frozen_string_literal: true

require_relative 'base'

module MmBotApi
  class Client < Base
    def me
      make_request(:get, 'users/me')
    end

    def users
      page = 0
      users_array = []
      loop do
        response = make_request(:get, 'users', { page: })
        break if response.failure || response.body.empty?

        page += 1
        users_array += response.body
      end

      users_array
    end

    def create_direct(user_id)
      make_request(:post, 'channels/direct', [self_id, user_id])
    end

    def send_message(channel_id, message)
      body = {
        channel_id:,
        message:
      }
      make_request(:post, 'posts', body)
    end

    private

    def self_id
      @self_id ||= User[{ username: ENV.fetch("MM_BOT_NAME") }][:external_id]
    end
  end
end
