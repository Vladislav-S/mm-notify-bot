# frozen_string_literal: true

module MmBotApi
  class Response
    attr_reader :response

    def initialize(response)
      @response = response

      logger.info("#{self.class.name} | status: #{response.code} | body: #{body}")
    end

    def body
      @body ||= JSON.parse(response.body)
    end

    def failure
      !success
    end

    def success
      response.code == "200" || response.code == "201"
    end

    def error_message
      return unless failure
      body["message"]
    end

    def error_code
      return unless failure

      response.code
    end

    private

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
