# frozen_string_literal: true

require_relative 'response'

module MmBotApi
  class Base
    CURRENT_API = "/api/v4/".freeze

    private

    def make_request(method, path, params = {})
      request_url = URI(base_url + CURRENT_API + path + '?')

      case method
      when :post
        post_request(request_url, params)
      when :get
        get_request(request_url, params)
      end
    end

    def get_request(request_url, params)
      query_params = params.map { |key, val| "#{key}=#{val}" }.join("&")
      request_url.query = query_params
      logger.info("#{self.class.name} GET #{request_url}")
      Net::HTTP.start(request_url.host, request_url.port, use_ssl: true) do |http|
        request = Net::HTTP::Get.new(request_url)
        request['Content-Type'] = 'application/json'
        request['accept'] = 'application/json'
        request['Authorization'] = "Bearer #{token}"

        Response.new(http.request(request))
      end
    rescue Exception => e
      logger.error(e)
    end

    def post_request(request_url, body)
      logger.info("#{self.class.name} POST #{request_url} | #{body}")

      Net::HTTP.start(request_url.host, request_url.port, use_ssl: true) do |http|
        request = Net::HTTP::Post.new(request_url)
        request['Content-Type'] = 'application/json'
        request['accept'] = 'application/json'
        request['Authorization'] = "Bearer #{token}"
        request.body = body.to_json unless body.empty?

        Response.new(http.request(request))
      end
    end

    def base_url
      @base_url ||= ENV.fetch("MM_URL").freeze
    end

    def token
      @token ||= ENV.fetch("MM_BOT_TOKEN").freeze
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
