# frozen_string_literal: true

namespace :yt do
  desc "Fetch users from Mattermost"

  task :fetch_queues do
    require_relative '../../config/config'

    FetchYtQueues.new.perform
  end

  task :create_triggers do
    require_relative '../../config/config'

    CreateTriggers.new.perform
  end
end
