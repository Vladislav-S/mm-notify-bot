# frozen_string_literal: true

namespace :db do
  desc "Run migrations"

  task :migrate do
    # если все реквайрменты подключить в рейк файле - не соберется с нуля миграция тк. запрашивается шема таблиц
    # TODO: разобраться с реквайрементами
    require "sequel/core"
    require 'dotenv'
    require 'logger'
    Dotenv.load
    Sequel.extension :migration
    DATABASE_HOST = ENV.fetch("DATABASE_HOST").freeze
    DATABASE_PORT = ENV.fetch("DATABASE_PORT").freeze
    DATABASE_NAME = ENV.fetch("POSTGRES_DB").freeze
    DATABASE_USER = ENV.fetch("POSTGRES_USER").freeze

    connection_params = {
      adapter: :postgres,
      user: DATABASE_USER,
      host: DATABASE_HOST,
      port: DATABASE_PORT,
      logger: Logger.new($stderr)
    }

    Sequel.connect(connection_params.merge(database: DATABASE_NAME)) do |db|
      if ENV["ENVIROMENT"] == "production"
        Sequel::Migrator.run(db, "db/migrations")
      else
        version = ENV.fetch("VERSION", 9999999).to_i
        Sequel::Migrator.run(db, "db/migrations", target: version)
      end
    end
  end
end
