# frozen_string_literal: true

namespace :users do
  desc "Fetch users from Mattermost"

  task :fetch do
    require_relative '../../config/config'

    FetchUsers.new.perform
  end
end
