# frozen_string_literal: true

module Handlers
  class Base
    def call(res)
      self.class.new.perform(res)
    end

    private

    def yt_api
      @yt_api ||= YtApi::Client.new
    end

    def mm_api
      @mm_api ||= MmBotApi::Client.new
    end

    def yt_host
      "https://tracker.yandex.ru/"
    end

    def ok_status
      [200, {}, []]
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
