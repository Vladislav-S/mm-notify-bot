# frozen_string_literal: true

require_relative 'base'

module Handlers
  class Notification < Base
    attr_reader :mention_comment

    def perform(raw_req)
      issue_id = JSON.parse(raw_req["rack.hijack"].body)["issue"]
      # FIXME: когда комментов много, нужно указывать страницу
      yt_request = yt_api.issue_comments(issue_id)
      return log_error("yt auth failed") if yt_request.unauthorized?

      @mention_comment = yt_request.body.last
      return log_error("no mention") if comment[:mention].nil?

      user = User[{ username: comment[:mention] }]
      return log_error("user not found") if user.nil?

      chat = user.chats.first || create_chat(user[:external_id], user)

      return log_error("message already exists") if message_already_exists(chat)

      mm_response = mm_api.send_message(chat[:external_id], notify_message)
      return log_error("failed to send message to MM") if mm_response.failure

      message_mm_id = mm_response.body["id"]

      chat.add_message({ yt_id: comment[:yt_id], mm_id: message_mm_id, text: notify_message })
    rescue Exception => e
      logger.error(e)
    ensure
      # FIXME: по какой то причине, в случае эксепшена, он судя по всему не возвращает норм статус для трекера
      # из-за чего тот начинает спамить вебхуками
      ok_status
    end

    private

    def comment
      return @comment if @comment
      @comment = {
        text: mention_comment["text"],
        mention: /@[A-Za-z_.-]+/.match(mention_comment["text"]).to_s[1..],
        yt_id: mention_comment["id"],
        issue_slug: mention_comment["self"][41..].split("/").first
      }
    end

    def create_chat(external_id, user)
      response = mm_api.create_direct(external_id)

      return ok_status if response.failure

      external_id = response.body["id"]

      # FIXME: по какой то причине на проде отваливалась привязка чата к пользователю
      chat = Chat[{ external_id: }]
      if chat.nil?
        chat_id = Chat.insert({ external_id: })
        chat = Chat[chat_id]
      end
      user.add_chat(chat)
      chat
    end

    def message_already_exists(chat)
      chat.messages.select { _1[:yt_id] == comment[:yt_id].to_s }.any?
    end

    def notify_message
      "Вас упомянули в [#{comment[:issue_slug]}](#{yt_host}#{comment[:issue_slug]}). #{comment[:text]}"
    end

    def log_error(message)
      logger.error(message)
      ok_status
    end
  end
end
