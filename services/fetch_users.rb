# frozen_string_literal: true

class FetchUsers
  def perform
    users = api.users
    users = users.map { { external_id: _1["id"], username: _1["username"] } }

    existing_user_external_ids = User.where(external_id: users.map { _1[:external_id] }).select_map(:external_id)
    users = users.reject { existing_user_external_ids.include? _1[:external_id] }

    User.multi_insert(users) if users.any?
  end

  private

  def api
    @api ||= MmBotApi::Client.new
  end
end
