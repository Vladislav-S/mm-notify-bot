# frozen_string_literal: true

class CreateTriggers
  TRGGER_DEFAULT_NAME = "notify_MM_bot_trigger"

  def perform
    yt_queues.each do |yt_queue|
      response = api.get_triggers(yt_queue[:external_key])

      next if response.failure || response.body.select { _1["name"] == TRGGER_DEFAULT_NAME }.any?
      puts "CREATING"
      create_response = api.create_trigger(yt_queue[:external_key], trigger_params)

      next if create_response.failure
      yt_queue.update(trigger_id: create_response.body["id"])
    end
  end

  private

  def api
    @api ||= YtApi::Client.new
  end

  def yt_queues
    @yt_queues ||= YtQueue.where(ignore: false, trigger_id: nil)
  end

  def trigger_params
    @trigger_params ||= {
      name: TRGGER_DEFAULT_NAME,
      actions: [{
        type: "Webhook",
        endpoint: ENV.fetch("YT_TRIGGER_ENDPOINT", ""),
        method: "POST",
        contentType: "application/json; charset=UTF-8",
        authContext: { type: "noauth" },
        body: "{\n\"issue\" : \"{{issue.id}}\"\n}"
      }],
      conditions: [
        { type: "Event.comment-create" },
        { type: "CommentStringMatchCondition", word: "@", ignoreCase: false, removeMarkup: false, noMatchBefore: false }
      ]
    }
  end
end
