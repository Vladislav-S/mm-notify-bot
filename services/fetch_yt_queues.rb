# frozen_string_literal: true

class FetchYtQueues
  def perform
    queues = api.queues
    queues = queues.map { { external_id: _1["id"], external_key: _1["key"], external_name: _1["name"] } }

    existing_queue_ids = YtQueue.where(external_id: queues.map { _1[:external_id] }).select_map(:external_id)
    queues = queues.reject { existing_queue_ids.include? _1[:external_id] }

    YtQueue.multi_insert(queues) if queues.any?
  end

  private

  def api
    @api ||= YtApi::Client.new
  end
end
