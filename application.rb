# frozen_string_literal: true

require_relative 'config/config'

Server.new.start
