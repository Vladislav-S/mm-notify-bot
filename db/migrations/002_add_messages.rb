Sequel.migration do
  up do
    create_table(:messages) do
      primary_key :id
      Integer :chat_id
      String :mm_id, null: false, unique: true
      String :yt_id, null: false, unique: true
      String :text
    end
  end

  down do
    drop_table(:messages)
  end
end
