Sequel.migration do
  up do
    create_table(:users) do
      primary_key :id
      String :username, null: false, unique: true
      String :external_id, null: false, unique: true
    end

    create_table(:users_to_chats) do
      primary_key :id
      Integer :user_id, null: false
      Integer :chat_id, null: false
    end

    create_table(:chats) do
      primary_key :id
      String :external_id, null: false, unique: true
    end
  end

  down do
    drop_table(:users)
    drop_table(:users_to_chats)
    drop_table(:chats)
  end
end
