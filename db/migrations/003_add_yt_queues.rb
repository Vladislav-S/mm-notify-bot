Sequel.migration do
  up do
    create_table(:yt_queues) do
      primary_key :id
      Integer :external_id, null: false, unique: true
      String :external_key, null: false, unique: true
      String :external_name
      Boolean :ignore, default: true
      Integer :trigger_id
    end
  end

  down do
    drop_table(:yt_queues)
  end
end
