# frozen_string_literal: true

# Sequel.connect(ENV.fetch("DATABASE_URL"), logger: Logger.new($stderr))

DATABASE_HOST = ENV.fetch("DATABASE_HOST").freeze
DATABASE_PORT = ENV.fetch("DATABASE_PORT").freeze
DATABASE_NAME = ENV.fetch("POSTGRES_DB").freeze
DATABASE_USER = ENV.fetch("POSTGRES_USER").freeze

connection_params = {
  adapter: :postgres,
  user: DATABASE_USER,
  host: DATABASE_HOST,
  port: DATABASE_PORT,
  logger: Logger.new($stderr)
}

DB = Sequel.connect(connection_params.merge(database: DATABASE_NAME))
Sequel::Model.db = DB
