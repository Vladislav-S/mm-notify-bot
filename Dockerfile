FROM ruby:3.2.0-bullseye AS notify-bot

ENV INSTALL_PATH /opt/app
RUN mkdir -p $INSTALL_PATH

RUN gem install bundler dotenv pg sequel agoo pry

WORKDIR /opt/app

COPY ./Gemfile ./
RUN gem update --system
RUN bundler install

COPY ./ ./

EXPOSE 8001

ENTRYPOINT [ "./entrypoint.sh" ]