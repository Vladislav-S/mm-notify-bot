# frozen_string_literal: true

class Server
  attr_reader :handler

  def initialize(port = 8001)
    Agoo::Server.init(port, 'root')
    @handler = Handlers::Notification.new
  end

  def start
    Agoo::Server.handle(:POST, "/notify-yt", handler)
    Agoo::Server.start
  end
end
