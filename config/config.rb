# frozen_string_literal: true

require_relative 'base'
require_relative 'development'

# CONFIGS
require_relative '../db/config'

# LIBS

require_relative '../lib/mm_bot_api/client'
require_relative '../lib/yt_api/client'

# MODELS
require_relative '../models/user'
require_relative '../models/message'
require_relative '../models/chat'
require_relative '../models/yt_queue'


# SERVICES
require_relative '../services/fetch_users'
require_relative '../services/fetch_yt_queues'
require_relative '../services/create_triggers'
# HANDLERS
require_relative '../handlers/notification'

# EXECUTABLE
require_relative '../server'
