# frozen_string_literal: true

require 'logger'

require 'dotenv'
Dotenv.load

require 'net/http'
require 'uri'
require 'json'
require 'pg'
require 'sequel'
require 'agoo'
